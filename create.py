import read_all
import json

data = {
    "date_created": "2021-03-01 17:00:04.252143", 
    "date_updated": "2021-03-01 17:00:04.252143", 
    "description": "This is a brand new course", 
    "discount_price": 5.0, 
    "id": 201, 
    "image_path": "images/some/path/foo.jpg", 
    "on_discount": False, 
    "price": 25.0, 
    "title": "Brand new course"
  }

json_data = read_all.load_data()	# Calling the JSON function to read the entire file and store it into variable

def save_data(json_data):
    with open(r'C:\Users\sseera\Desktop\Python\Workspace\JSON CRUD\addresses.json', 'w') as f:
        json.dump(json_data, f, indent=2)

save_data(json_data)

json_string=json.dumps(data)

with open(r'C:\Users\sseera\Desktop\Python\Workspace\JSON CRUD\addresses.json','w') as f:
  f.write(json_string)