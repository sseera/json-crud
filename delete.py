import read_all
import create

json_data = read_all.load_data()


id = input('Enter Id to Delete the item associated with it')
del json_data[id]


# and Finally Save the file with save_json_data function

create.save_data(json_data)
