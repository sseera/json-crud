import create
import read_all
json_data = read_all.load_data()

id = input('Enter Id to find course in json')
data = {
  "date_updated": "2021-03-01 17:11:20.005061", 
  "description": "New description", 
  "discount_price": 5.0, 
  "id": 201, 
  "image_path": "images/some/path/foo.jpg", 
  "on_discount": False, 
  "price": 25.0, 
  "title": "Blah blah blah"
}
json_data[id] = data # updating data
create.save_data(json_data) # saving the json file