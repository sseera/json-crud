import json

x = {
"name": "John",
"age": 30,
"married": True,
"divorced": False,
"children": ("Ann","Billy"),
"pets": None,
"cars": [
{"model": "BMW 230", "mpg": 27.5},
{"model": "Ford Edge", "mpg": 24.1}
]
}

x = json.dumps(x)
# parse x:

y = json.loads(x)

# the result is a Python dictionary:
print(y['age'])
print (y['cars'][1]['mpg'])